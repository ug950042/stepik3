# Projekt Markdown

To jest przykładowe repozytorium z plikiem README w języku Markdown.

## Paragrafy

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis auctor libero vitae leo dictum, in volutpat velit eleifend. Curabitur nec bibendum orci.

Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ut elit ac purus eleifend fermentum.

Maecenas euismod, odio eget varius eleifend, justo lacus cursus nisl, eu tincidunt dui leo vel sem.

## Stylizacja Tekstu

Tekst może być **pogrubiony**, *kursywą* lub ~~przekreślony~~.

## Cytat

> "Życie to ciągłe naukowe odkrywanie nowych rzeczy."

## Listy

1. Lista numerowana
   1. Podpunkt 1
   2. Podpunkt 2
2. Drugi punkt
   - Podpunkt nienumerowany 1
   - Podpunkt nienumerowany 2
![obraz](./images/obraz.png)
## Blok Kodu

Poniżej znajduje się przykładowy blok kodu w języku Python:

```python
def hello_world():
    print("Hello, World!")

hello_world()

