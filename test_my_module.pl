# test_my_module.py

import unittest
from my_module import multiply_numbers

class TestMyModule(unittest.TestCase):

    def test_multiply_numbers(self):
        result = multiply_numbers(2, 3)
        self.assertEqual(result, 6)

if __name__ == '__main__':
    unittest.main()
