# Issue Markdown Example

## Tabela

| Kolumna 1 | Kolumna 2 | Kolumna 3 |
|:---------:|:---------:|:---------:|
|   Wartość A   |   Wartość B   |   Wartość C   |
|   Wartość D   |   Wartość E   |   Wartość F   |
|   Wartość G   |   Wartość H   |   Wartość I   |

## Podkreślenie Składni w Pythonie

Poniżej znajduje się przykładowy kod programu w języku Python podkreślający składnię:

```python
def hello_world():
    print("Hello, World!")

hello_world()

Stopka (Footnote)

To jest zdanie z odnośnikiem do stopki1.
Oznaczone Nagłówki
Nagłówek 1 {#naglowek-1}

To jest treść nagłówka 1.
Nagłówek 2 {#naglowek-2}

To jest treść nagłówka 2.
Lista Zadań

 Zadanie 1
 Zadanie 2

     Zadanie 3

Użycie Emotikonów

🎉 Ładne osiągnięcie! :smiley:
Użycie Emotikonów (zewnętrzne źródło)

👍 Sprawdź to! :rocket: (źródło: GitHub Emojis)
